
cmake_minimum_required(VERSION 3.13)

project(benchmark)

find_package(lwtnn REQUIRED)

add_executable(stresstest src/stresstest.cxx src/test_utilities.cxx)
target_link_libraries(stresstest lwtnn)
